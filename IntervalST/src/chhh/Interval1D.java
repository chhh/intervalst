package chhh;

import com.sun.istack.internal.NotNull;
import com.sun.javafx.beans.annotations.NonNull;

public class Interval1D<V extends Comparable<V>> implements Comparable<Interval1D<V>> {
    public final V lo;   // left endpoint
    public final V hi;  // right endpoint

    // precondition: left <= right
    public Interval1D(V left, V right) {
        if (left.compareTo(right) > 0)
            throw new RuntimeException("Illegal interval: left < right");
        this.lo = left;
        this.hi = right;
    }

    // does this interval intersect that one?
    public boolean intersects(Interval1D<V> that) {
        if (that.hi.compareTo(this.lo) < 0) return false;
        if (this.hi.compareTo(that.lo) < 0) return false;

        return true;
    }

    // does this interval a intersect b?
    public boolean contains(V x) {
        return (lo.compareTo(x) <= 0) && (x.compareTo(hi) <= 0);
    }

    @Override
    public int compareTo(Interval1D<V> that) {
        if      (this.lo.compareTo(that.lo) < 0) return -1;
        else if (this.lo.compareTo(that.lo) > 0) return +1;
        else if (this.hi.compareTo(that.hi) < 0) return -1;
        else if (this.hi.compareTo(that.hi) > 0) return +1;
        else return 0;
    }

    public String toString() {
        return "[" + lo + ", " + hi + "]";
    }




    // test client
    public static void main(String[] args) {
        Interval1D<Integer> a = new Interval1D<>(15, 20);
        Interval1D<Integer> b = new Interval1D<>(25, 30);
        Interval1D<Integer> c = new Interval1D<>(10, 40);
        Interval1D<Integer> d = new Interval1D<>(40, 50);

        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("d = " + d);

        System.out.println("b intersects a = " + b.intersects(a));
        System.out.println("a intersects b = " + a.intersects(b));
        System.out.println("a intersects c = " + a.intersects(c));
        System.out.println("a intersects d = " + a.intersects(d));
        System.out.println("b intersects c = " + b.intersects(c));
        System.out.println("b intersects d = " + b.intersects(d));
        System.out.println("c intersects d = " + c.intersects(d));

    }

}
